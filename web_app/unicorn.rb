#!/usr/bin/env ruby

# Set path to app that will be used 
if ENV["LOLTROVE_PROD"]
  @source_dir = "/var/lib/loltrove"
  @run_dir = "/var/run/loltrove"
  @log_dir = "/var/log/loltrove"
else
  @source_dir = "."
  @run_dir = "./tmp"
  @log_dir = "tmp/log"
end

worker_processes 4
working_directory @source_dir

timeout 30

# Specify path for unicorn socket
listen 5000, :backlog => 64

# Set process id path
pid "#{@run_dir}/pids/unicorn.pid"

# Only log to file if specified, otherwise use stdout
if ENV["LOG_TO_FILE"]
  stderr_path "#{@log_dir}/unicorn/stderr.log"
  stdout_path "#{@log_dir}/unicorn/stdout.log"
end
