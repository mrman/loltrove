#!/usr/bin/env bash

# Make socket directory
mkdir -p /tmp/unicorn/sockets/
unicorn -c unicorn.rb -E production -D

# Kill
# cat tmp/pids/unicorn.pid | xargs kill -QUIT