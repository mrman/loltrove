module.exports = function(grunt) {

    // Project config
    grunt.initConfig({
	pkg: grunt.file.readJSON('package.json'),

	// JSHint
	jshint: {
	    default: {
		ignores: [],
		exclude: [],
		src: ['static/js/*.js']
	    }
	},

	// Uglify
	uglify: {
	    options: {
		banner: '/* <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
	    },
	    default: {
		files: {
		    'static/dist/js/<%= pkg.name %>.<%= pkg.version %>.min.js': [
                        'static/plugins/jquery/jquery-1.10.2.min.js',
                        'static/plugins/freezeframe/freezeframe.js',
                        'static/js/<%= pkg.name %>.js'
		    ],
		}
	    }
	},

	// cssmin
	cssmin: {
	    default: {
		src: 'static/css/<%= pkg.name %>.css',
		dest: 'static/dist/css/<%= pkg.name %>.<%= pkg.version %>.min.css'
	    }
	},

	// CSSLint
	csslint: {
	    strict: {
		src: 'static/style/css/*.css'
	    }
	},

	// Watch
	watch: {
	    js: {
		files: ['static/js/*.js'],
		tasks: ['jshint']
	    },
	    cssmin: {
		files: ['static/style/css/*.css'],
		tasks: ['cssmin']
	    },
	    csslint: {
		files: ['static/style/css/*.css'],
		strict: {
		    src: 'static/style/css/*.css',
		    tasks: ['csslint']
		}
	    },
	}
        
    });

    // Load various plugins for the tasks
    grunt.loadNpmTasks('grunt-css');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');


    // Default tasks
    grunt.registerTask('default',['jshint','uglify','csslint','cssmin','watch']);
    grunt.registerTask('build',['jshint','uglify','csslint','cssmin']);

};
