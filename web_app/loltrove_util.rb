###### Loltrove utility functions

require 'erb'

# DB Schema verification vars
LOL_KEYS = ['url','name','tags']

# Common message responses
MSG_INVALID_SESSION = "Uh Oh! Looks like you have an invalid session -- Please log (out and) in again."
MSG_SERVER_ERROR = "Something unexpected happened on the server... please try again later. If the problem persists, please email contact@loltrove.io"

# Validate the different kinds of items that belong in the database
def is_valid_lol?(type,json_data)
  return false if not ITEM_TYPES.include? type
  
  keys = json_data.keys
  
  # Ensure the json data only contains the keys we expect
  return false if ((LOL_KEYS - keys) + (keys - LOL_KEYS)).length > 0

  case type
  when "videos"
    #    puts "Validating a video!"
    return true
  when "images"
    #    puts "Validating an image!"
    return true
  end

  return true
end

# Remove front-end properties from various object types
def trim_front_end_properties(type,json_data) 
  return false if not ITEM_TYPES.include? type

  # Depending on the item type, trim JSON data that doesn't need to be stored in the database
  # This should be split up based on type, later
  return json_data.keep_if {|k,_| LOL_KEYS.include? k }

end

# Check if session key is valid
def is_valid_session_key?(session_key)
  # Ensure that the session key is valid 
  session_objs = Array(r.table('sessions').filter({:session_key => session_key}).run())
  return session_objs.length != 0,session_objs.first
end

# Parse JSON and swallow errors (maybe add some logging later)
def parse_JSON(request_body_stream)
  begin
    json_data = JSON.parse request_body_stream
  rescue JSON::ParserError
    halt 500, {:status => 'error', :message => 'Invalid request, failed to parse JSON payload.'}
  end
  return json_data
end

# Get total query duration from rethinkdb profiler output
def total_query_duration(response)
  response['profile'].map { |t| t['duration(ms)'] }.reduce(:+)
end

# Mail new users the welcome message (and their API key)
def mail_new_user(user_info, api_key)
  template_body = ERB.new File.read 'views/new_user.email.erb'
  puts "INFO: Sending new user email to new user #{user_info["email"]}, host: #{RDB_HOST}"
  Mail.deliver do
    from 'loltrove-noreply@vadosware.io'
    to user_info["email"]
    subject 'Welcome to the loltrove community!'
    body template_body.result(binding)
  end
end


