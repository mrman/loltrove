# Loltrove API (and landing page)

Loltrove is an application for saving various gifs and funny pictures one comes across on the internet. As users can choose to sync their data to loltrove's servers, an API component is required.


# Getting started developing loltrove

0. Download development dependencies ([RVM](https://rvm.io/), [Ruby 2.3.0](http://www.ruby-lang.org/en/), [bundler](http://bundler.io/))
1. `bundle exec ruby loltrove.rb`

# Local build

Local building is handled with docker -- See the `Dockerfile` contained in the repository

To build most recent image:

`make docker-image`

To run the container:

`make docker-instance`


# Deployment

## Requirements

The machine that loltrove is deployed to should have at a minimum:

- Docker installed and running
- A running instance of [rethinkdb](http://rethinkdb.com/), available at ports loltrove expects
- Internal firewalls configured properly to enable communication between host and containers over the appropriate port for RethinkDB
- A reverse proxy or web server pointing (upstream) to the default port (5000) of loltrove

## Procedure

Deployment of loltrove is simple:

1. The docker image that has been built is transferred to the specified host (make SSH_ADDR arg) over SSH
2. The docker image is run over SSH

Deployment of the docker image has been packaged into the following make commands:

`make docker-transfer-to-host [BUILT_IMAGE=<image id>] SSH_ADDR=user@host`
`make docker-deploy-on-host SSH_ADDR=user@host`

These two commands may be run together with:

`make docker-deploy-to-host SSH_ADDR=user@host`
