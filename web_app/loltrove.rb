#!/usr/bin/env ruby

# Test production
#set :environment, :production

#Globals
ITEM_TYPES = ['videos','images']
SEARCH_TYPES = ['tag','namedesc']

require 'sinatra'
require 'pp'
require 'sinatra/reloader'
require 'rethinkdb'
require 'json'
require 'net/http'
require 'mail'
require 'tilt/erb'

require_relative 'loltrove_util.rb'

# Reloader settings
also_reload './loltrove_util.rb'

# Set up app constants
LOLTROVE_DEFAULT_DB = "loltrove"
RDB_PORT = 28015
RDB_HOST = ENV["RDB_PORT_#{RDB_PORT}_TCP_ADDR"] || 'localhost'
RDB_DB = ENV["LOLTROVE_DB_NAME"] || LOLTROVE_DEFAULT_DB

if RDB_HOST.nil?
  raise Exception.new "Invalid/missing RDB_HOST"
end

# RethinkDB server setup & connect
begin
  include RethinkDB::Shortcuts
  r.connect(:host => RDB_HOST, :port => RDB_PORT, :db => 'loltrove').repl
rescue Exception => e
  puts "ERROR: Init/Connection to the database failed... (please ensure the database server is running)!"
  raise e
end

# Custom SMTP connection to be used for mail
Mail.defaults do
  delivery_method :smtp, address: "vadosware.io", host: $RDB_HOST, port: 587
end

class LoltroveApp < Sinatra::Base

  configure :production, :development do
    enable :logging

    # Whitelist chrome extension
    enable :protection
    set :protection, origin_whitelist: ["chrome-extension://ebbckpkgcmgmbnhfoachimbgahlgajnm","chrome-extension://lljdihgeafddbnlggpibhdkkhmlgilfn"]

  end


  set :public_folder, File.dirname(__FILE__) + '/static'

  get '/' do
    erb :landing
  end

  # Handle user logout
  post '/api/logout' do
    post_data = parse_JSON(request.body.read)

    if post_data['email'].nil? or post_data['session_key'].nil?
      halt 400, {:status => 'error', :message => 'Email/Session key not specified...' }.to_json
    end

    # Delete the session
    delete_result = r.table('sessions').filter({:email => post_data['email'],
                                                 :session_key => post_data['session_key']}).delete().run()


    if delete_result['deleted'] > 0 or (delete_result['deleted'] == 0 and delete_result['errors'] == 0)

      # Save a record of the logout if user agent is provided (best effort -- maybe log if this is working at some point)
      if post_data.has_key?('user_agent')
        metrics_save_result = r.table('session_stats').insert({:user_agent => post_data['user_agent'],
                                                                :action => 'LOGOUT',
                                                                :session_key => post_data['session_key'],
                                                                :time => Time.now.to_i}).run()
      end

      # Succeed on deletion or non-existent session (generally the only reason for nothing to be deleted but no errors with this query)
      return {:status => 'success',
        :message => "Successfully logged out user [#{post_data['email']}]" }.to_json
    else
      halt 400, {:status => 'error',
        :message => "Failed to log out user [#{post_data['email']}]" }.to_json
    end

  end


  # Handle user login
  post '/api/login' do
    # Require email and api key
    post_data = parse_JSON(request.body.read)
    if post_data['email'].nil? or post_data['api_key'].nil?
      err = {:status => 'error', :message => 'Email and/or API key not specified'}
      halt 400, err.to_json
    end

    email,api_key = post_data['email'],post_data['api_key']

    #Find the user with that email and api_key,
    found_users = Array(r.table('users').filter({:email => email, :api_key => api_key}).run())

    if found_users.empty?
      halt 401, {:status => 'error', :message => 'Invalid email/API key combination'}.to_json
    elsif found_users.length > 1
      # Should never get here, email + API key combinations should be unique
      halt 500
    else

      # Generate session key
      api_alphabet = (0..9).to_a + ((97..122).to_a + (65..90).to_a).map {|n| n.chr }
      session_key = (0..32).map {|c| api_alphabet[rand(api_alphabet.length)]}.join

      # Add the session to the list of current sessions
      insert_result = r.table('sessions').insert({:email => post_data['email'], :session_key => session_key}).run()



      # Return successful login, along with generated session key
      if insert_result['inserted'] > 0

        # Save record of the user's login if user agent is provided (best effort -- maybe log if this is working at some point)
        if post_data.has_key?('user_agent')
          metrics_save_result = r.table('session_stats').insert({:user_agent => post_data['user_agent'],
                                                                  :action => 'LOGIN',
                                                                  :session_key => session_key,
                                                                  :time => Time.now.to_i}).run()
        end

        return {:status => 'success',
          :message => 'Successfully logged in',
          :user => found_users[0],
          :session_key => session_key
        }.to_json
      else
        halt 500, {:status => 'error',
          :message => 'Login failed... it looks like the server is having some issues. If this persists, please contact support@vadosware.com'}.to_json
      end
    end

  end


  # Register a username for a user
  # This endpoint is not currently being used, but if usernames are adopted then it may be used to provide usernames
  post '/api/usernames' do
    post_data = parse_JSON(request.body.read)
    if post_data['username'].nil?
      halt 400, {:status => 'error', :message => 'Username not specified'}.to_json
    end

    email,username = "test",post_data['username']

    # Update the user object
    result= r.branch(r.table('users').filter({:username => username}).count().eq(0).run(),
                     r.table('users').filter({:email => email}).update({:username => username}),
                     nil).run()

    # Check if the username existed, or insert failed for  some other reason
    if result.nil?
      halt 412, {:status => 'error', :message => 'User with that name already exists'}.to_json
    elsif result["replaced"] != 1 or result['errors'] > 0
      halt 500,{:status => 'error', :message => 'An error was encountered trying to create your user account... Please wait a while and try again'}.to_json
    end

    return {:status => 'success',
      :message => 'Successfully updated username to [#{username}]'}.to_json

  end

  # Handle registration of user accounts
  post '/api/register' do
    content_type :json
    post_data = parse_JSON(request.body.read)

    # Require email
    if post_data['email'].nil? or post_data['email'].strip! == ''
      halt 400, {:status => 'error', :message => 'Email not specified'}.to_json
    end

    email = post_data['email'].strip

    # Create user object and enter it into the database (only if there isn't someone else with the same username)
    result= r.branch(r.table('users').filter({:email => email}).count().eq(0).run(),
                     r.table('users').insert({:email => email}),
                     nil).run()

    # Check if the email belongs to someone else, or insert failed for  some other reason
    if result.nil?
      halt 412, {:status => 'error', :message => 'User with that email address already exists'}.to_json
    elsif result["inserted"] != 1 or result['errors'] > 0
      halt 500,{:status => 'error', :message => 'An error was encountered trying to create your user account... Please wait a while and try again'}.to_json
    end

    # Generate an API key for a new user & update new user's entry
    new_user_id = result['generated_keys'][0]
    api_alphabet = (0..9).to_a + ((97..122).to_a + (65..90).to_a).map {|n| n.chr }
    new_api_key = (0..32).map {|c| api_alphabet[rand(api_alphabet.length)]}.join
    update_result = r.table('users').get(new_user_id).update({:api_key => new_api_key}).run()
    updated_user_account = update_result["replaced"] == 1

    # Initialize  & insert a userinfo object for the new user
    new_user_info = {
      "email" => email,
      "user_id" => new_user_id,
      "favorite_images" => [],
      "favorite_videos" => []
    }
    insert_info_result = r.table('userinfo').insert(new_user_info).run()
    inserted_user_info = insert_info_result["inserted"] == 1

    # Send API key to new user via email
    begin
      mail_new_user(new_user_info, new_api_key)
    rescue
      # Rollback the user updates
      r.table('users').get(new_user_id).delete().run()
      r.table('userinfo').filter({:user_id => new_user_id}).delete().run()
      puts "ERROR: Failed to send user email! Rolled back user creation for user with email #{email}\n",$!
      return {:status => 'error',
        :message => 'An error was encountered sending your welcome email... please ensure that a valid email address has been used and try again!'}.to_json
    end

    #Notify of success/failure
    if updated_user_account and inserted_user_info then
      return {:status => 'success', :message => 'Account successfully created, Please check your mailbox (and spam folder) for your API key!'}.to_json
    else
      puts "ERROR: updated_user_account = #{updated_user_account} , inserted_user_info = #{inserted_user_info}"
      return {:status => 'error', :message => 'An error occurred while creating your user account...'}.to_json
    end

  end


  # Enable requests for resending API key for a given email address, once a day
  post '/api/resend_api_key' do
    content_type :json
    post_data = parse_JSON(request.body.read)

    # Require email
    if post_data['email'].nil? or post_data['email'].strip! == ''
      halt 400, {:status => 'error', :message => 'Email not specified'}.to_json
    end

    email = post_data['email'].strip

    # Get the user's API key
    user_infos = r.table('users').filter({:email => email}).coerce_to('array').run()

    # Ensure there is only one user found, and resend API key
    if user_infos.length == 0 then
      halt 401, {:status => "error", :message => "Could not find a user with the specified email address"}.to_json
    elsif user_infos.length > 1 then
      halt 500, {:status => "error", :message => "An error was encountered trying to fetch your API key. Please email support@loltrove.io."}.to_json
    else
      user_info = user_infos.first
      current_resend_time = Time.now.to_i
      time_since_last_resend = current_resend_time - (user_info["last_api_key_resend"] or 0)
      # Ensure last resend was at least 24H ago (86400 seconds)
      if time_since_last_resend < 86400 then
        halt 401, {:status => "error", :message => "Last resend request was within 24 hours... Please wait 24 hours before requesting another API email."}.to_json
      else
        mail_new_user(user_info, user_info["api_key"])
        r.table('users').get(user_info["id"]).update({"last_api_key_resend" => current_resend_time}).run()
        return {:status => "success", :message => "API Key has been emailed to [#{email}]"}.to_json
      end
    end

  end


  ###### Tagging

  # Add tags
  put '/api/tags/:tag_str/:item_type/:item_id' do
    content_type :json
    # Ensure the item_type is part of the accepted list of item types
    halt 400, {:status => 'error', :message => 'Invalid item type'}.to_json if not ITEM_TYPES.include?(params[:item_type])

    # Ensure that the item exists in it's specified table
    insert_result = r.branch(r.table('tags').filter(:item_id => params[:item_id]).count().eq(1),
                             r.table('tags').insert({:tag_str => params[:tag_str],
                                                      :item_type => params[:item_type],
                                                      :item_id => :item_id}),
                             nil).run()

    # Ensure that the thing exists
    halt 412, {:status => 'error', :message => 'Could not find item with given ID'}.to_json if insert_result.nil?

    # Return result if the tag has been inserted properly
    tag_inserted = insert_result['inserted'] == 1
    if tag_inserted then
      return {:status => 'success', :message => 'Successfully tagged'}.to_json
    else
      halt 500, {:status => 'success', :message => 'Failed to tag the item with the given ID'}.to_json
    end

  end

  # Remove tags
  delete '/api/tags/:tag_str/:item_type/:item_id' do
    content_type :json
    # Ensure the item_type is part of the accepted list of item types
    halt 400, {:status => 'error', :message => 'Invalid item type'} if not ITEM_TYPES.include?(params[:item_type])

    # Attempt to delete the tag if it exists
    delete_result = r.table('tags').filter({:tag_str => params[:tag_str],
                                             :item_type => params[:item_type],
                                             :item_id => :item_id
                                           }).delete().run()

    deleted_tag = delete_result['deleted'] == 1

    if deleted_tag
      return {:status => 'success', :message => 'Successfully deleted tag'}.to_json
    else
      halt 500, {:status => 'error', :message => 'Failed to delete tag'}.to_json
    end

  end


  ###### Uploading

  # Add a new item (video/image)
  put %r{/api/(images|videos)/new} do
    content_type :json
    request.body.rewind
    item_type = params[:captures].first
    json_data = parse_JSON(request.body.read)

    # Ensure that the json data has the expected keys
    if not (json_data.has_key?('lol_data') and json_data.has_key?('session_key'))
      halt 400, {:status => 'error', :message => 'Invalid request'}.to_json
    end

    # Pull the lol out of the JSON data, and validate it (after removing unneeded front-end properties)
    item = trim_front_end_properties(item_type, json_data['lol_data'])
    if not is_valid_lol?(item_type,item)
      halt 500, {:status => 'error', :message => 'Failed to upload item to loltrove.io, JSON has invalid format/data'}.to_json
    end

    # Ensure that the session key is valid
    if !is_valid_session_key?(json_data['session_key'])
      halt 400, {:status => 'error', :message => MSG_INVALID_SESSION}.to_json
    end

    # Find the user with the given session key
    user = r.table('sessions').filter({:session_key => json_data['session_key']})
      .eq_join("email",r.table('users'),index="email").pluck('right').coerce_to('object').run()
    halt 400, {:status => 'error', :message => MSG_INVALID_SESSION}.to_json if user.nil?

    # Check if an image with the same URL exists, if so, update
    item[:uploader_id] = user[:id]
    insert_result = r.branch(r.table('images').filter({:url => item['url']}).count().eq(0),
                             r.table(item_type).insert(item),
                             r.table(item_type).filter({:url => item['url']}).update() { |i|
                               {:tags => (i['tags'] + item['tags']).distinct }
                             }).run()

    # Insert the item, if validation passes (result will be nil if invalid)
    inserted_or_updated = insert_result["inserted"] == 1 || insert_result["replaced"] == 1

    # Save record of insert/replacement
    if json_data.has_key?('user_agent')
      action = inserted_or_updated ? (insert_result["inserted"] ? 'INSERTED_NEW_LOL' : 'UPDATED_EXISTING_LOL') : 'FAILED_UPLOAD'
      metrics_save_result = r.table('images_stats').insert({:user_agent => json_data['user_agent'],
                                                             :action => action,
                                                             :session_key => session_obj['session_key'],
                                                             :time => Time.now.to_i}).run()
    end

    # Report status and result
    if inserted_or_updated
      return {:status => 'success', :message => 'Upload successful'}.to_json
    else
      halt 500, {:status => 'error', :message => "Failed to upload given item... Please ensure the is a valid #{item_type} and try again later"}.to_json
    end

  end

  ###### Loading

  # Get an new item (video/image) by ID
  get %r{/api/(images|videos)/([\w-]+)} do
    content_type :json
    item_type,item_id =  params[:captures]
    json_data = parse_JSON(request.body.read)

    # Only serve valid session_key holders
    if not json_data.has_key?('session_key') or !is_valid_session_key?(json_data['session_key'])
      halt 400, {:status => 'error', :message => MSG_INVALID_SESSION}.to_json
    end

    # Get the item from the database by id
    item = r.table(item_type).get(item_id).run()

    # Report status & result
    if item.nil?
      return {:status => 'error', :message => 'Failed to fetch item with the specified ID'}.to_json
    else
      return {:status => 'success',
        :message => 'Successfully fetched item',
        :item => item }.to_json
    end
  end

  # Retrieve items (using POST only because of JSON data payload containing session id)
  post %r{/api/(images|videos)/} do
    content_type :json
    item_type = params[:captures].first
    json_data = parse_JSON(request.body.read)

    # Only serve valid session_key holders
    if not json_data.has_key?('session_key') or !is_valid_session_key?(json_data['session_key'])
      halt 400, {:status => 'error', :message => MSG_INVALID_SESSION}.to_json
    end

    # Pull options
    limit,offset = 30,0
    if json_data.has_key?('options')
      limit = json_data['options']['limit'].to_i if json_data['options'].has_key?('limit')
      offset = json_data['options']['offset'] if json_data['options'].has_key?('offset')
    end


    # Only serve valid session_key holders
    if not json_data.has_key?('session_key') or !is_valid_session_key?(json_data['session_key'])
      halt 400, {:status => 'error', :message => MSG_INVALID_SESSION}.to_json
    end

    items = r.table(item_type)[offset .. limit + offset].limit(limit).run()

    return {:status => 'success',
      :message => 'Successfully fetched items',
      :items => Array(items)
    }.to_json
  end


  ###### Search

  get '/api/search' do
    content_type :json
    query = request['q']
    type = request['type']
    query.strip!

    # Return nothing on empty queries
    if query.nil? or query.empty?
      return {:status => 'success',
        :related_objects => []}.to_json
    end

    # Use the type to determine which kind of search to perform and perform it.
    if type.nil? or type == 'namedesc'
      # Look through the submitted descriptions to find something that matches in name/desc
      objects_with_phrase = r.table('descriptions').filter{ |d| d[:name].match(query) | d[:desc].match(query) }.run()
      return {:status => 'success',
        :search_type => 'namedesc',
        :related_objects => Array(objects_with_phrase)}.to_json
    elsif type == 'tag'
      # Look through the submitted tags to find something that matches tokenized search string
      tokens = query.split

      # Optimize this later, KISS for now (tried a little bit with eqjoins, didn't work out so well
      images_with_tag = r.table('images').filter {|img| img[:tags].set_union(tokens).count().gt(0) }.run()
      videos_with_tag = r.table('videos').filter {|img| img[:tags].set_union(tokens).count().gt(0) }.run()

      return {:status => 'success',
        :search_type => 'namedesc',
        :related_objects => Array(videos_with_tag) + Array(images_with_tag) }.to_json
    else
      # Only allow specified search types
      halt 400, {:status => 'error', :message => 'Invalid request, please specify a valid search type.'}.to_json
    end

  end

  ###################
  # Image reporting #
  ###################

  post %r{/api/report/(videos|images)} do
    content_type :json
    item_type = params[:captures].first
    json_data = parse_JSON(request.body.read)

    # Only serve valid session_key holders
    if not json_data.has_key?('session_key') or !is_valid_session_key?(json_data['session_key'])
      halt 400, {:status => 'error', :message => MSG_INVALID_SESSION}.to_json
    end

    # Ensure lol has been provided
    halt 400, {:status => 'error', :message => 'Lol object is missing.'}.to_json if not json_data.has_key?('lol_data')
    item = trim_front_end_properties(item_type, json_data['lol_data'])
    halt 400, {:status => 'error', :message => 'Item object is invalid.'}.to_json if not is_valid_lol?(item_type,item)

    # Perform join if item is valid
    join_result = r.branch(r.table('images').filter({:url => item["url"]}).count().eq(0),
                           nil,
                           r.table('images').filter({:url => item["url"]}).inner_join(r.table('users')) { |img,usr|
                             img[:uploader_id].eq(usr[:id]) }.coerce_to('array')).run()


    # If there is no matching image, error out, else add the report
    if join_result.nil? then
      halt 400, {:status => "error", :message => "Item object is invalid"}.to_json
    else
      # Get reporter ID
      reporter = r.table('sessions').filter({:session_key => json_data['session_key']})
        .eq_join("email",r.table('users'),:index => "email")[0]["right"].run()

      # Expecting only one result from the images should be unique by url (index)
      halt 500, {:status => "error", :message => MSG_SERVER_ERROR} if join_result.length != 1
      image = join_result[0]['left']
      uploader = join_result[0]['right']

      # If image exists but no report, report the item along with the user that created it, else increment count of reports
      report_record = {:reporter => reporter["id"], :reported_time => Time.now.to_i}
      r.branch(r.table('reports').filter({:image_url => image["url"]}).count().eq(0),
               # Insert new report
               r.table('reports').insert({:image_url => image["url"],
                                           :uploader_id => uploader["id"],
                                           :report_times => [report_record],
                                           :count => 0}),
               # Update reports & report times
               r.table('reports').filter({:image_url => image["url"]})
                 .update {|r|
                 {:count => r[:count]+1,
                   :report_times => r[:report_times].append(report_record)} }).run()
    end

    # Report successful ending
    return {:status => "success",
      :message => "Successfully reported image"}.to_json
  end

  ###### Image fetching
  # This needs to be implemented in the future -- possibly handled by background tasks to implement a caching layer

  # get '/api/image/fetch/' do
  #   post_data = JSON.parse request.body.read
  #   url = post_data['url']
  #   api_key = post_data['api_key']

  #   # Ensure URL and API key have been specified
  #   if url.nil? or api_key.nil?
  #     content_type :json
  #     halt 400, {:status => 'error', :message => 'No URL specified'}.to_json if url.nil?
  #     halt 400, {:status => 'error', :message => 'Invalid/Missing API key'}.to_json if api_key.nil?
  #   end

  #   # Get the image from the url, save in tmp and return byte stream
  #   img = Net::HTTP.get(URI(url))
  #   content_type detect_content_type(url)
  #   return img

  # end

  # def detect_content_type(url)
  #   return 'image/gif'
  # end

  # Run if ruby file is executed directly (dev)
  run! if app_file == $0
end
