// Loltrove Chome App JS

// Globals
LOLTROVE_API_ADDR = 'https://loltrove.io/api';

// Angular app init
app = angular.module("loltrove",[]).
    config(function($routeProvider) {
        $routeProvider.when('/login',{
            templateUrl: 'templates/login.html',
            controller: 'LoginController'
        });

        $routeProvider.when('/lols',{
            templateUrl:'templates/lols.html',
            controller: 'LolsController',
            resolve: LolsController.resolve
        });
        
        $routeProvider.when('/lols/new',{
            templateUrl:'templates/lols_new.html',
            controller: 'LolsNewController'
        });

        $routeProvider.otherwise({ redirectTo: '/lols' });
    });

// Services
app.service("UserService", function($http) {
    var usr_svc = {
        init: function() {
            usr_svc.loadHiddenLols(function(saved_hidden_lols) {
                hidden_lols = saved_hidden_lols;
            });
        },

        isAuthenticated: function() {
            return localStorage.getItem("currentUser") != undefined;
        },

        getAPIKey: function() {
            return localStorage.getItem("apiKey");
        },

        setUser: function(u) {
            localStorage.setItem("apiKey", u.api_key);
            localStorage.setItem("currentUser",u.username);
            localStorage.setItem("currentUserEmail",u.email);
        },
        
        currentUser: function() {
            return localStorage.getItem("currentUser");
        },

        currentUserEmail: function() {
            return localStorage.getItem("currentUserEmail");
        },
        
        endSession: function() {
            localStorage.removeItem("apiKey");
            localStorage.removeItem("currentUser");
            localStorage.removeItem("remoteSessionKey");
        },

        setRemoteSessionKey: function(key) {
            localStorage.setItem("remoteSessionKey",key);
        },

        getRemoteSessionKey: function() {
            return localStorage.getItem("remoteSessionKey");
        },

        login: function(email,api_key,callback) {
            var svc = this;
            // Build the login data object that will be passed to the server
            var login_data = {email: email,
                              api_key: api_key};
            if (!navigator.doNotTrack && 'userAgent' in navigator && navigator.userAgent !== '' ) 
                login_data.user_agent = navigator.userAgent

            // Send email & api_key to server, retrieve session key
            $http.post(LOLTROVE_API_ADDR + '/login',login_data).success(function(data,status) {
                if (data.status === 'success') {
                    svc.setUser(data.user);
                    svc.setRemoteSessionKey(data.session_key);
                }
                if (!_.isUndefined(callback)) { callback(data); }
            }).error(function(data,status) {
                if (!_.isUndefined(callback)) { callback(data); }
            });
        },

        signup: function(email,callback) {
            $http.post(LOLTROVE_API_ADDR + '/register',{email: email}).then(function(xhr) {
                // If user has successfully been registered, let them know:
                if (!_.isUndefined(callback)) callback(xhr.data)
            },function(xhr) {
                if (!_.isUndefined(callback)) callback(xhr.data)
            });
        },

        resend_api_key: function(email,callback) {
            $http.post(LOLTROVE_API_ADDR + '/resend_api_key',{email:email}).then(function(xhr) {
                if (!_.isUndefined(callback)) callback(xhr.data)
            },function(xhr) {
                if (!_.isUndefined(callback)) callback(xhr.data)
            });
        },

        logout: function(callback) {
            var svc = this;
            // Build the logout data object that will be passed to the server
            var logout_data = {email: this.currentUserEmail(), 
                               session_key: this.getRemoteSessionKey()}
            if (!navigator.doNotTrack && 'userAgent' in navigator && navigator.userAgent !== '' ) 
                logout_data.user_agent = navigator.userAgent

            $http.post( LOLTROVE_API_ADDR + '/logout',logout_data).success(function(data,status){
                svc.endSession();
                if (!_.isUndefined(callback)) callback(data);
            }).error(function(data,status){
                if (!_.isUndefined(callback)) callback(data);
            });
        }

    };
    return usr_svc;
});

// Alert service
app.service('AlertService',function() {
    var alerts = [];
    var svc = {
        // Add a new alert
        addAlert: function(alert,scope) {
            // Quit if the alert doesn't have the required fields
            if (!('type' in alert) || !('message' in alert) ||
                typeof alert.type === 'undefined' || typeof alert.message === 'undefined')
                return;

            // Push the alert into the list of alerts
            alerts.push(alert);

            // Add timed delete functionality
            if ('timed_delete' in alert) {
                var timeout_len = (alert.timed_delete === 'number') ? alert.timed_delete : 3000;
                setTimeout(function() { 
                    // Dismiss the alert & update the scope it came from
                    svc.dismissAlert(alert);
                    _.defer(function(){scope.$apply()});
                }, timeout_len);
            }

        },

        // Dismiss an alert
        dismissAlert: function(alert) {
            alerts = _.without(alerts,alert);
        },

        // Retrieve list of alerts
        getAlerts: function() {
            return alerts;
        }
        
    };
    return svc;
});

// Image service
app.service('ImageService', function($http,UserService) {
    // Initialize localstorage for images if it does not exist yet
    var images = {};
    var remote_images = {};
    var hidden_lols = {};
    var trash = [];
    var initialized = false;
    var img_svc =  {
        
        _runtimeListener: null,
        event_callbacks: {},
        remote_image_offset: 0,
        
        // Initialize the image service, possibly forcing it to re-pull lols
        init: function(force) {
            // Don't init more than once
            if ((_.isUndefined(force) || !force) && initialized) {return;}

            // Load images
            chrome.storage.local.get(['loltrove_images','hidden_lols'],function(items) {
                if (!_.has(items,'loltrove_images')) {
                    // Initialize chrome storage list of images
                    chrome.storage.local.set({'loltrove_images': {}});
                } else {
                    // Load images from chrome storage
                    img_svc.loadAllLols();
                }


            });

            // Register runtime listener
            this.registerRuntimeListener();
            
            initialized = true;
        },

        // Get local/remote images
        getLocalImages: function() { return _.values(images); },
        getRemoteImages: function() { return _.values(remote_images); },

        // Check if an image is local or remote
        isLocalImage: function(img) { return _.has(images,img.url); },
        isRemoteImage: function(img) { return _.has(remote_images,img.url); },

        // Clear local/remote images
        clearLocalImages: function() { images = {}; },
        clearRemoteImages: function() { 
            remote_images = {};
            img_svc.remote_image_offset = 0; 
        },

        // Save current images to long term memory (and update the images array)
        saveCurrentImages: function(scope,callback) {
            chrome.storage.local.set({'loltrove_images': images},function() {

                // Update the scope of whatever made the change
                if (typeof scope !== 'undefined' && scope) 
                    _.defer(function(){ scope.$apply(); });
                img_svc.triggerCallbacks('images_changed');

                // Perform callback if specified
                if (!_.isUndefined(callback))
                    callback();

            });
        },

        // Add an local image 
        addImage: function(img,scope,callback) {
            images[img.url] = img;
            if (typeof scope !== 'undefined' && scope !== null) 
                img_svc.saveCurrentImages(scope);
            else 
                img_svc.saveCurrentImages();
            if (!_.isUndefined(callback)) { callback(img) }
            
            // Trigger callbacks
            img_svc.triggerCallbacks('images_changed');
        },

        // Add a remote (community uploaded) image to local storage
        saveRemoteImageLocally: function(img,callback) {
            img._origin = 'local';
            img_svc.addImage(img,null,function() {

                // Perform passed callback
                if (!_.isUndefined(callback) && !_.isNull(callback)) { callback(img); }

                // Alert everyone to new image
                img_svc.triggerCallbacks('images_changed');
            });
        },

        // Toggle favorite setting on an image
        toggleFavoriteImage: function(img,callback) {
            if (img.url in images) {
                images[img.url]['isFavorite'] = ('isFavorite' in images[img.url] ) ? !images[img.url]['isFavorite'] : true;
            } else {
                // If the image is not already local, save it locally
                img_svc.saveRemoteImageLocally(img,function(img) {
                    images[img.url]['isFavorite'] = ('isFavorite' in images[img.url] ) ? !images[img.url]['isFavorite'] : true;
                });
                
            }
            // Save current images (w/ updated/locally saved favorite)
            img_svc.saveCurrentImages();
            // Handle alerts & add new favorite image to context menu
            var bg_page =chrome.extension.getBackgroundPage();
            if (img.isFavorite) { 
                bg_page.addFavoriteImage(img); 
            } else {
                bg_page.removeFavoriteImage(img);
            }
            
            if (!_.isUndefined(callback) && callback !== null) { callback(img); }
        },


        // Delete an image from the service
        deleteImage: function(image,scope,callback) {
            // Delete the image from the local cache & save 
            if (img_svc.isLocalImage(image)) { 
                // Keep a copy of the image that was just deleted in the trash, just in case person wants to undo
                trash.push(image);
                delete images[image.url]; 
                img_svc.saveCurrentImages(scope,function() {
                    if (!_.isUndefined(callback)) { callback(image); }
                });
                img_svc.triggerCallbacks('images_changed');
            }
        },

        // Undo the last image deletion
        undoLastImageDeletion: function(img,scope,callback) {
            if (trash.length == 0) {
                return;
            }

            // Attempt to add the re-add the image, and if that succeeds, remove it from the trashReove the image from the trash
            img_svc.addImage(img,scope,function(img) { 
                var img_index = _.indexOf(trash,img);
                if (img_index > -1) {
                    trash.splice(img_index,1);
                    _.defer(function(){ scope.$apply(); });
                    if (!_.isUndefined(callback)) { callback(img); }
                }
            });
        },
        
        // Retrieve locally stored images
        loadLocalImages: function(callback) {
            chrome.storage.local.get('loltrove_images', function(items) {
                // Augment the images with their origin and execute callback
                var stored_images =  _.map(_.values(items['loltrove_images']), function(i) { return _.extend(i,{_origin:'local'}) });
                // Add all the images to interior container & trigger callbacks to alert others of changes
                _.each(stored_images,function(i) { images[i.url] = (i) });
                img_svc.triggerCallbacks('images_changed');
                if (!_.isUndefined(callback)) callback(stored_images);
            });
        },

        // Retrieve images stored on loltrove servers
        loadRemoteImages: function(options,callback) {
            var session_key = UserService.getRemoteSessionKey();
            query_options = {session_key: session_key};
            if (typeof options !== 'undefined' && _.size(options) > 0) 
                query_options.options = options;
            else 
                query_options.options = {offset:img_svc.remote_image_offset};

            // Quit early if the user is not authenticated (endpoint will reject)
            if (typeof session_key === 'undefined' || session_key == null) {
                if (!_.isUndefined(callback)) callback([]);
                return;
            }

            // Load remote images then call callback
            // NOTE - Endpoint without specifing query options *SHOULD* return a reasonable # of the most recently added lols)
            $http.post(LOLTROVE_API_ADDR + '/images/',
                       query_options).then(
                           function(xhr) {
                               // Filter out remote images that are already there, rest will be filtered by view
                               var downloaded_images = _.filter(xhr.data.items,function(i) { return !img_svc.isRemoteImage(i); });

                               downloaded_images = _.map(downloaded_images,function(i){ return _.extend(i,{_origin:'community'}); });
                               _.each(downloaded_images,function(i) { remote_images[i.url] = i; });

                               // Update the remote image offset
                               img_svc.remote_image_offset += downloaded_images.length;

                               // Update listeners
                               img_svc.triggerCallbacks('images_changed');

                               // Callback w/ the images that were added
                               if (!_.isUndefined(callback)) callback(downloaded_images);
                           });
        },

        // Load all types of images 
        loadAllLols: function(callback) {
            // load local saved images 
            img_svc.loadLocalImages(function() {
                // Load hidden images
                img_svc.loadHiddenLols();
                
                // Get remote images (if logged in)
                if (UserService.isAuthenticated()) {
                    img_svc.loadRemoteImages({},callback);
                } else {
                    if (!_.isUndefined(callback)) callback(images);
                }
            });

        },

        // Remote images search
        searchRemoteImages: function(query,callback) {
            $http.get(LOLTROVE_API_ADDR + '/search',
                      {params: {q: query, type: 'tag'}
                      }).success(function(data,status) {
                          // Augment downloaded images & remove images already available locally (if there are collisions)
                          var downloaded_images = _.filter(data.related_objects ,function(img) {return !(img.url in images || img.url in remote_images);});
                          downloaded_images = _.map(downloaded_images, function(img) { return _.extend(img,{_origin:'community'}); });

                          if (!_.isUndefined(callback) && callback != null) callback(downloaded_images);
                      }).error(function(data,status) {
                          if (!_.isUndefined(callback) && !_.isNull(callback)) callback([]);
                      });
        },


        /////////////////
        // Hidden Lols //
        /////////////////

        // Check if a lol is in the hidden list
        isHiddenLol: function(img) { return img.url in hidden_lols; },

        getHiddenLols: function() { 
            return _.values(hidden_lols); 
        },

        toggleHideLol: function (img,callback) {
            if (img_svc.isHiddenLol(img)) {
                img_svc.unhideLol(img,callback);
            } else {
                img_svc.hideLol(img,callback);
            }
        },

        // Add to list of lols that should be hidden from the user (reported/hidden community lols)
        hideLol: function(img,callback) {
            if (!_.has(hidden_lols,img.url)) {
                hidden_lols[img.url] = img;
                img_svc.saveHiddenLols();
            } 
            img_svc.triggerCallbacks('images_changed');
            if (!_.isUndefined(callback) && !_.isNull(callback)) callback(img,true);
        },

        // Unhide a lol that user had hidden
        unhideLol: function(img,callback) {
            if (_.has(hidden_lols,img.url)) {
                delete hidden_lols[img.url];
                img_svc.saveHiddenLols();
            }
            img_svc.triggerCallbacks('images_changed');
            if (!_.isUndefined(callback) && !_.isNull(callback)) callback(img,false); 
        },
        
        // Clear hidden lols list
        clearHiddenLols: function() { hidden_lols = {}; },

        // Persist current list of hidden lols
        saveHiddenLols: function() {
            chrome.storage.local.set({'hidden_lols':hidden_lols});
        },

        // Load current set of hidden lols from storage
        loadHiddenLols: function() {
            // Don't load hidden lols if not signed in
            if (!UserService.isAuthenticated()) {return [];}
            chrome.storage.local.get('hidden_lols',function(items) {
                // If storage contains some hidden lols, load them
                if (_.has(items,'hidden_lols')) {
                    hidden_lols = items['hidden_lols'];
                }
            });
        },

        // Report a lol as inappropriate
        reportRemoteLol: function (lol,callback) {
            var report_data = {session_key: UserService.getRemoteSessionKey(),
                               lol_data: lol};
            
            // Report lol
            $http.post(LOLTROVE_API_ADDR + '/report/images', report_data).then(function(xhr) {
                // Pass the status along to the callback (for possible use in alert)
                if (!_.isUndefined(callback) && callback !== null) { callback(xhr.data); }
            },function(xhr) {
                if (!_.isUndefined(callback) && callback !== null) { callback(xhr.data); }
            });
        },
        

        // Get Image that has been saved to disk
        getImageByURL: function(url) {
            // Get the current list of images
            return chrome.storage.local.get('loltrove_images', function(items) {
                var images = items.images;
                // <TODO> Check the cache and maybe insert 'cached_data' attribute
                if (url in images) {
                    return images[url];
                } else {
                    return null;
                }
            });
            
        },

        // Register runtime listener to sync changes to lols from other places (ex. adding while browsing)
        registerRuntimeListener: function() {
            if (img_svc._runtimeListener == null) {
                img_svc._runtimeListener = function(msg, sender, sendResponse) {
                    if ('type' in msg) {
                        switch (msg.type) {
                            
                        case 'LOL_ADD':
                            // Add the image
                            img_svc.addImage(msg.data);
                            break;

                        default: 
                            return;
                        }
                        
                    }
                };
                chrome.runtime.onMessage.addListener(img_svc._runtimeListener);
            }
        },

        // Allow parts to register callbacks to more easily inform of changes
        registerCallback: function(event,callback) {
            if (!(event in img_svc.event_callbacks)) { img_svc.event_callbacks[event] = [] }

            img_svc.event_callbacks[event].push(callback);
        },
        
        // Call all callbacks for a given event (and pass data, if provided)
        triggerCallbacks: function(event,data) {
            // Special cases then general event handling
            if (event === 'images_changed' && _.isUndefined(data)) {
                // Special action, if event is images changed, pass along default data
                var data = {'local':img_svc.getLocalImages(),
                            'remote':img_svc.getRemoteImages(),
                            'hidden':img_svc.getHiddenLols()};
                _.each(img_svc.event_callbacks[event],function(c){ c(data); });
            } else if (_.has(img_svc.event_callbacks, event)) {
                // Default action, pass along provided data with callbacks
                _.each(img_svc.event_callbacks[event],function(c){ c(data); });
            }
        }

    };
    img_svc.init();
    return img_svc;
});

/****** Controllers ******/
function AlertsController($scope,AlertService,ImageService) {
    $scope.alerts = AlertService.getAlerts;
    $scope.dismiss_alert = AlertService.dismissAlert;
}

function NavController($scope,$http,$location,AlertService,UserService,ImageService) {
    $scope.logout = function() { 
        UserService.logout(function(response) {
            // Upon successful logout, clear remote images
            ImageService.clearRemoteImages();
            ImageService.clearHiddenLols();

            var alert = {type: response.status,
                         message: response.message}
            if (!(response.status === 'error')) {
                alert.timed_delete = 3000
                $location.path('/login');
            }
            AlertService.addAlert(alert,$scope);
        }); 
    }
    $scope.isAuthenticated = UserService.isAuthenticated();
}

function LoginController($scope,$http,$location,AlertService,UserService,ImageService) {
    // Redirect if already authenticated
    if (UserService.isAuthenticated()){ $location.path('/lols'); };
    
    // Setup scope
    $scope.login = function() { 
        // Attempt to login, if successful
        UserService.login($scope.email,$scope.api_key,function(response) {
            // Alert user to success/failure (add a timer if the response was not an error)
            var alert = {type: response.status,
                         message: response.message}
            if (!(response.status === 'error')) 
                alert.timed_delete = 3000
            AlertService.addAlert(alert,$scope);
            
            // Redirect if logged in successfully
            if (response.status == 'success') {
                ImageService.init(true);
                $location.path('/lols');
            }
        });
        
    };

    // Handle signup
    $scope.signup = function() { 
        UserService.signup($scope.email, function(response) {
            // Alert user to success/failure
            var alert = {type: response.status,
                         message: response.message};
            
            // In the case of error, allow user to attempt to resend the API key
            if (response.status === 'error') {
                alert.actions = [{text: 'Resend API Key',
                                  click_fn: function() {
                                      // Send a request for resend of API key
                                      console.log("scope.email = ",$scope.email);
                                      UserService.resend_api_key($scope.email,function(resp) {
                                          AlertService.addAlert({type:resp.status,
                                                                 message: resp.message},$scope);
                                      });
                                  },
                                  icon_class: 'fa fa-envelope'
                                 }];
            }
            AlertService.addAlert(alert,$scope);
        });
    };
}

// Handle listing of lols
function LolsController($scope,$location,$timeout,AlertService,UserService,ImageService,images) {
    $scope.authenticated = UserService.isAuthenticated();
    if ($scope.authenticated)
        $scope.user = UserService.currentUser();
    
    // Register callback with the image service so we can update on change (outside of angular)
    ImageService.registerCallback('images_changed',function(images) {
        // Update local, remote & hidden lols lists (Will jumble if you simply overwrite, do diff & update to prevent jumbling)
        $scope.images = diff_and_update_list($scope.images,images.local);
        $scope.remote_images = diff_and_update_list($scope.remote_images,images.remote);
        $scope.hidden_lols = diff_and_update_list($scope.hidden_lols,images.hidden);
        $scope.$apply();
    });

    // Get the local and remote images (if logged in)
    $scope.images = ImageService.getLocalImages();
    $scope.remote_images = ImageService.getRemoteImages(); 
    $scope.hidden_lols = ImageService.getHiddenLols(); 

    // Handle showing/hiding local and community images
    $scope.showLocalImagesTab = true;
    $scope.showCommunityImagesTab = $scope.authenticated;
    $scope.showHiddenLolsTab = false;

    // Image functions
    $scope.toggleHideLol = function(img) { 
        // Hide image
        ImageService.toggleHideLol(img,function(img,hidden) {
            var alert = {type: "success",
                         message: "[" + img.name + "] has been " + (hidden ? " hidden " : " restored"),
                         timed_delete: true,
                         actions: [
                             {text: 'Undo',
                              click_fn: function() {
                                  // Unhide the image
                                  ImageService.unhideLol(img);
                                  AlertService.dismissAlert(alert);
                              },
                              icon_class: 'fa fa-undo'
                             }]
                        };
            AlertService.addAlert(alert,$scope);
        });
    };

    $scope.reportRemoteLol = function(img) { 
        // Hide then report the image
        ImageService.hideLol(img, function(img) {
            ImageService.reportRemoteLol(img,function(status) {
                var alert = {type: status.status,
                             message: status.message,
                             timed_delete: status.status === 'success'};
                AlertService.addAlert(alert,$scope);
            });
        }); 
    };

    $scope.deleteImage = function(img) {

        ImageService.deleteImage(img,$scope,function(img) {
            // delete the contextmenu entry
            var bg_page = chrome.extension.getBackgroundPage();
            bg_page.removeContextMenuLol(img);

            // Add alert to signal deletion (and allow user to undo)
            var alert = {
                type: 'success',
                message: "[ " + img.name + " ] deleted.",
                timed_delete: true,
                actions: [
                    {text:'Undo', 
                     click_fn: function() {
                         // Call the image service's undo function
                         ImageService.undoLastImageDeletion(img,$scope);
                         AlertService.dismissAlert(alert);
                     },
                     icon_class: 'fa fa-undo'
                    }
                ]
            };
            AlertService.addAlert(alert,$scope);
            
        });
    };
    
    // Toggle whether an image is favorited
    $scope.toggleFavoriteImage = function(img) {
        ImageService.toggleFavoriteImage(img,function(img) {
            var alert = {
                timed_delete: 2000,
                type: ((img.isFavorite) ? 'success' : 'warning'),
                message: "[" + img.name + "] " + ((img.isFavorite) ? "added to favorites!" : "removed from favorites!"),
                actions: [
                    {text: "Undo",
                     click_fn: function() {
                         // Toggle the image back
                         ImageService.toggleFavoriteImage(img);
                         AlertService.dismissAlert(alert);
                     },
                     icon_class: 'fa fa-undo'
                    }
                ]
            };
            AlertService.addAlert(alert,$scope);
        });
    };

    // Convert a community image to a local one
    $scope.saveRemoteImageLocally = function(img) {
        ImageService.saveRemoteImageLocally(img,function(img) {
            var alert = {
                type: 'success',
                message: "Saved community image [ " + img.name + " ] locally.",
                timed_delete: true,
                actions: [
                    {text: 'Undo',
                     click_fn: function() {
                         // Remove the image that has been added
                         ImageService.deleteImage(img);
                         AlertService.dismissAlert(alert);
                     },
                     icon_class: 'fa fa-undo'
                    }
                ]
            };
            AlertService.addAlert(alert,$scope);

        });
    };

    // Search remote images 
    $scope.remoteSearch = function(query) {
        // Show some asynchronous loader?
        ImageService.searchRemoteImages(query,function(images) {
            $scope.remote_images = $scope.remote_images.concat(images);
        });
    };

    // Load more images
    $scope.loadMoreLols = function(img) {
        ImageService.loadRemoteImages({},function(images){
            if (images.length == 0) {
                var alert = {
                    type: 'error',
                    message: "Oh noes! Looks like there are no more lols to be fetched! :/ -- Get out there and find and share some viral gold!",
                };
                AlertService.addAlert(alert,$scope);
            } else {
                // New images are returned in the images array, but safest way is to update scope to match what's in service
                $scope.remote_images = ImageService.getRemoteImages();
            }
        });
    };

    // Clipboard modification functions
    $scope.copyImageUrlToClipboard = function(img) {
        var bg_page = chrome.extension.getBackgroundPage();
        bg_page.copyTextToClipboard(img.url);
        // Send an alert to show we copied!
        var alert = {
            type: 'success',
            message: 'Successfully copied URL for [ ' + img.name + ' ] to clipboard!',
            timed_delete: 2000
        };
        AlertService.addAlert(alert,$scope);
    };

}    

LolsController.resolve = {
    images: function($q, ImageService,UserService) {
        var deferred = $q.defer();

        // If there are no images, initialize UserService and ImageService
        if (ImageService.getLocalImages().length == 0) { 
            ImageService.init(); 
        }

        deferred.resolve();
        return deferred.promise;
    },
    
    delay: function($q,$timeout) {
        var delay = $q.defer();
        $timeout(delay.resolve,0);
        return delay.promise;
    }
};

// Handle creation of Lols
function LolsNewController($scope,$location,$http,$routeParams,AlertService,UserService,ImageService) {
    $scope.name = $routeParams.name || '';
    $scope.url = $routeParams.url || '';
    $scope.isFavorite = $routeParams.isFavorite || true;
    $scope.addToTrove = $routeParams.addToTrove || true;
    $scope.autoclose = $routeParams.autoclose || false;

    $scope.add_lol = function() {
        lol_data = {name: $scope.name, 
                    url: $scope.url, 
                    isFavorite: $scope.isFavorite,
                    tags: $scope.name.trim().split(/\s+/),
                    _origin: 'local'};
        ImageService.addImage(lol_data,$scope, function() {

            // If user has opted to share, send update to server
            if ($scope.addToTrove) {
                var trove_data = {lol_data: lol_data, 
                                  session_key: UserService.getRemoteSessionKey()};
                if (!navigator.doNotTrack && 'userAgent' in navigator && navigator.userAgent !== '' ) 
                    trove_data.user_agent = navigator.userAgent

                $http.put(LOLTROVE_API_ADDR + '/images/new',trove_data).success(function(data,status) {
                    var alert = {type: data.status, message: data.message, timed_delete: 2000};
                    AlertService.addAlert(alert);
                }).error(function(data,status) {
                    var alert = {type: data.status, message: data.message};
                    AlertService.addAlert(alert);
                });
            }

            // If the image was a favorite, add it to the loltrove context menu right away
            var bg_page = chrome.extension.getBackgroundPage();
            if ($scope.isFavorite) {
                bg_page.addContextMenuLol(lol_data);
            } 

            // Broadcast addition of LOL
            chrome.runtime.sendMessage({type: "LOL_ADD",
                                        msg: "Just added a lol while surfing the web",
                                        data: lol_data}, function(response) { });

            // Close if autoclose (this should be set if someone is adding a lol while surfing the internets)
            if ($scope.autoclose) {
                var bg_page = chrome.extension.getBackgroundPage();
                var notification_data = {title: 'Loltrove - New Lol Added',
                                         msg:"Successfully saved new lol!",
                                         cancelDelay: 2000};
                bg_page.showNotification(notification_data,function() {
                    window.close();
                });
            } else {
                $location.path('/lols');
            }
            
        });

    };

}

/****** Custom directives ******/

// For navbar
app.directive('topNavbar', function() {
    return {
        replace: true,
        restrict: 'E',
        templateUrl: 'templates/top_nav.html'
    };
});

// Gif wall image
app.directive('gifWall', function() {
    return {
        replace: true,
        restrict: 'E',
        scope: {
            images: '=',
            query: '=',
            copyImageUrlToClipboard: '&',
            deleteImage: '&',
            toggleFavoriteImage: '&',
            saveRemoteImageLocally: '&',
            hideImage: '&',
            toggleHideLol: '&',
            reportRemoteLol: '&'
        },
        templateUrl: 'templates/gif_wall.html',
        link: function(scope,element,attrs) {
            scope.showHidden = attrs.showHidden;
            scope.showLocal = attrs.showLocal;
        }
    };
});


// For alert list
app.directive('alerts', function() {
    return {
        replace: true,
        restrict: 'E',
        templateUrl: 'templates/alerts_component.html'
    };
});

// Rudimentary implementation of buttons that serve as links
app.directive('btnLink', function($location) {
    return function (scope, element, attrs) {
        var path;
        
        attrs.$observe ('btnLink', function(val) {
            path = val;
        });

        element.bind('click',function () {
            scope.$apply( function() {
                $location.path(path);
            });
        });
    };
});

// Catch enter events
app.directive('ngEnter',function() {
    return function(scope,element,attrs) {
        element.bind("keydown keypress",function (event) {
            if (event.which === 13) {
                scope.$apply(function() {
                    scope.$eval(attrs.ngEnter);
                });
                event.preventDefault();
            }
        });
    }
});

// Focus on a given item
app.directive('firstFocus',function() {
    return function(scope,element,attrs) {
        element[0].focus();
    }
});

// Enable bootstrap tooltips
app.directive('bsTooltip',function() {
    return function(scope,element,attrs) {
        $(element[0]).tooltip();
    }
});


/****** Custom filters ******/

app.filter('capitalize', function() {
    return function(str, scope) {
        if (_.isUndefined(str)) {return true;}
        if (!_.isNull(str)) {
            return str.substring(0,1).toUpperCase()+str.substring(1);
        } else {
            return null;
        }
    }
});

// Filter to ensure that an image is not hidden
app.filter('notHiddenLol', function(ImageService) {
    return function(images,showHidden) {
        if (_.isUndefined(images)) {return images}
        if (showHidden) {
            return images;
        } else {
            return _.reduce(images,function(l,i) { return (!ImageService.isHiddenLol(i) ? l.concat(i) : l); }, []);
        }
    }
});

// Filter to ensure that an image is not local
app.filter('notLocalLol', function(ImageService) {
    return function(images,showLocal) {
        if (_.isUndefined(images)) {return images}
        if (!_.isUndefined(showLocal) && showLocal === 'false') {
            return _.reduce(images,function(l,i) { return (!ImageService.isLocalImage(i) ? l.concat(i) : l); }, []);
        } else {
            return images;
        }
    }
});


///////////////////////
// Utility functions //
///////////////////////

// Helper function to diff & update a list (Only push in the changed/new objects)
function diff_and_update_list(old_list,new_list) {
    var new_images = _.difference(new_list,old_list);
    var removed_images =  _.difference(old_list,new_list);
    return _.difference(old_list.concat(new_images),removed_images); 
}
