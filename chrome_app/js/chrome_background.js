/* Chrome App stuff */

// Global references
images_by_name = {}

// Create context menus after installation & startup
chrome.runtime.onInstalled.addListener(function() { createContextMenus(); });
chrome.runtime.onStartup.addListener(function() { createContextMenus(); });

// Create loltrove context menu
function createContextMenus() {
    var contexts=["image","editable"];

    chrome.contextMenus.create({"title": "Loltrove", 
                                "contexts": contexts,
                                "id":"loltrove-master"});
    chrome.contextMenus.create({"title": "Add Lol",
                                "contexts": ["image"],
                                "parentId":"loltrove-master", 
                                "id":"loltrove-add-lol"});
    chrome.contextMenus.create({"title": "Clipboard copy",
                                "contexts": ["editable"],
                                "parentId":"loltrove-master", 
                                "id":"loltrove-paste-lol"});
    chrome.contextMenus.create({"title": "Favorite lols",
                                "enabled": false,
                                "contexts": ["editable"],
                                "parentId":"loltrove-paste-lol", 
                                "id":"loltrove-paste-lol-header"});
    chrome.contextMenus.create({"type": "separator",
                                "contexts": ["editable"],
                                "parentId":"loltrove-paste-lol", 
                                "id":"loltrove-paste-lol-header-separator"});

    // Get the in-memory list of lols and list them for pasting
    chrome.storage.local.get('loltrove_images',function(items) {
        if ('loltrove_images' in items) {
            // Add favorite images to the context menu
            for (url in items.loltrove_images) {
                var image = items.loltrove_images[url];
                addFavoriteImage(image,null);
            }
        }
    });

}

// Setup click context manager for 
chrome.contextMenus.onClicked.addListener(function(info, tab) {
    // Handle lol adding
    if (info.parentMenuItemId === "loltrove-master" && info.menuItemId === 'loltrove-add-lol') {
        // Create a popup to loltrove, letting the person put in the name
        chrome.windows.create({url:'/loltrove.html#/lols/new?autoclose=true&url=' + info.srcUrl, 
                               focused: true,
                               width: 600,
                               height: 500,
                               left: 500,
                               top: 200,
                               type: 'popup'});
    }

    // Handle lol pasting
    if (info.parentMenuItemId === 'loltrove-paste-lol') {
        // Get the actual lol of the lol
        var lol_name = info.menuItemId.replace('loltrove-paste-link','')

        // Copy the url for the lol to the clipboard
        if (lol_name in images_by_name) {
            copyTextToClipboard(images_by_name[lol_name].url);
        }
    }

});


/*** Utility functions ***/

// Add an image to the favorite images context menu on the fly
function addFavoriteImage(img,callback) {
    var added_images = 0;
    images_by_name[img.name] = img;
    if ('isFavorite' in img && img.isFavorite) {
        chrome.contextMenus.create({"title": img.name,
                                    "contexts": ["editable"],
                                    "parentId":"loltrove-paste-lol",
                                    "id":"loltrove-paste-link" + img.name,
                                   });
        added_images++;
    } 
    if (typeof callback !== 'undefined' && callback !== null) { callback(added_images); }
}

// Remove an image from the favorite images context menu on the fly
function removeFavoriteImage(img,callback) {
    var removed_images = 0;
    if ('isFavorite' in img && !img.isFavorite) {
        chrome.contextMenus.remove("loltrove-paste-link" + img.name)
        removed_images++;
    }
    if (typeof callback !== 'undefined' && callback !== null) { callback(removed_images); }
}

function copyTextToClipboard(text,callback) {
    var copyFrom = $('<textarea/>');
    copyFrom.text(text);
    $('body').append(copyFrom);
    copyFrom.select();
    document.execCommand('copy', true);
    copyFrom.remove();
    if (typeof callback !== 'undefined' && callback !== null) { callback(); }
}

function showNotification(data,callback) {
    chrome.notifications.create('',{
        type: 'basic',
        iconUrl: '/img/icon.png',
        title: data.title,
        message: data.msg}, function(id) {
            if ('cancelDelay' in data) {
                setTimeout(function() {
                    chrome.notifications.clear(id,function(){});
                },data.cancelDelay);
            }
            callback();
        });
}

// Add a lol to the context menu
function addContextMenuLol(lol) {
    // Add the image to the in-memory list of images (indexed by name)
    // This is where the lookup is done from later
    images_by_name[lol.name] = lol;

    // Add the image to the context menu
    if (lol.name in images_by_name) {
        removeContextMenuLol(lol);
    }


    chrome.contextMenus.create({"title": lol.name,
                                "contexts": ["editable"],
                                "parentId":"loltrove-paste-lol",
                                "id":"loltrove-paste-link" + lol.name,
                               });
}

// Remove a lol from the context menu
function removeContextMenuLol(lol) {
    // Remove the context menu item for a lol (ex. after deletion)
    chrome.contextMenus.remove('loltrove-paste-link' + lol.name, function() {
    })
}

